---

# Who's this guy

+++

@snap[west]
@css[bio-about](Automation Consultant<br>Conference Speaker)
<br><br><br>
@fa[terminal fa-3x bio-fa](lazy)
<br><br><br>
@css[bio-about](Geeky Nerd<br>Programmer)
@snapend

@snap[east bio]
@css[bio-headline](Andrew Krug Medina)
<br>
@css[bio-byline](<i class='fab fa-github'></i> lazycoderio <i class='fab fa-twitter'></i>)
<br>
<img src="template/img/lazycoder-octopus-no-background.png" class="half-pic" />
<br>
@css[bio-tagline](A face made for radio!)
@snapend
